This docker creates a mongodb, it server as a local storage for user´s transactions


empty template

    {
        "id": "miguelangel.lopez.martinez@bbva.com",
        "name": "Miguel Angel",
        "apellidos": "Lopez Martinez",
        "banks": [
            {
                "name": "Blue Bank",
                "accounts": [
                    {
                        "id": 1,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "0",
                                "currency": "€",
                                "description": "Account",
                                "listado": []
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "0",
                                "currency": "€",
                                "description": "Credit Card",
                                "listado": []
                            },
                            {
                                "id": 3,
                                "type": "Credit Card",
                                "balance": "0",
                                "currency": "€",
                                "description": "Restaurant Card",
                                "listado": []
                            },
                            {
                                "id": 4,
                                "type": "Credit Card",
                                "balance": "0",
                                "currency": "€",
                                "description": "Transport Card",
                                "listado": []
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "0",
                                "currency": "€",
                                "description": "Account",
                                "listado": []
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Red Bank",
                "accounts": [
                    {
                        "id": 2,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "0",
                                "currency": "€",
                                "description": "Account",
                                "listado": []
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Orange Bank",
                "accounts": [
                    {
                        "id": 1,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "0",
                                "currency": "€",
                                "description": "Account",
                                "listado": []
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "0",
                                "currency": "€",
                                "description": "Credit Card",
                                "listado": []
                            }
                        ]
                    }
                ]
            }
        ]
    }



{
    "id":"miguelangel.lopez.martinez@bbva.com",
    "bank":"Blue Bank",
    "account": 1,
    "product": 1,
    "date": "26/11/2017",
    "amount": -33.95,
    "currency": "€",
    "description": "Compra Amazon"
}


[
    {
        "_id": {
            "$oid": "5a37981df36d2869668c2790"
        },
        "id": "miguelangel@bbva.com",
        "name": "Miguel Angel",
        "apellidos": "Lopez Martinez",
        "banks": [
            {
                "name": "Blue Bank",
                "accounts": [
                    {
                        "id": 1,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "5000",
                                "currency": "€",
                                "description": "Account",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 33.95,
                                        "description": "A",
                                        "status": "Confirmed"
                                    }
                                ]
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "1200",
                                "currency": "€",
                                "description": "Credit Card",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 20.19,
                                        "description": "Amazon",
                                        "status": "Confirmed"
                                    }
                                ]
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "200",
                                "currency": "€",
                                "description": "Restaurant Credit Card",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 10,
                                        "description": "Restaurante Domino",
                                        "status": "Confirmed"
                                    }
                                ]
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "200",
                                "currency": "€",
                                "description": "Transport Credit Card",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 12.2,
                                        "description": "Combinado Metronorte",
                                        "status": "Confirmed"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "5000",
                                "currency": "€",
                                "description": "Account",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 33.95,
                                        "description": "A",
                                        "status": "Confirmed"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Red Bank",
                "accounts": [
                    {
                        "id": 1,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "-500000",
                                "currency": "€",
                                "description": "Account",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 33.95,
                                        "description": "A",
                                        "status": "Confirmed"
                                    }
                                ]
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "1200",
                                "currency": "€",
                                "description": "Credit Card",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 20.19,
                                        "description": "Amazon",
                                        "status": "Confirmed"
                                    }
                                ]
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "200",
                                "currency": "€",
                                "description": "Restaurant Credit Card",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 10,
                                        "description": "Restaurante Domino",
                                        "status": "Confirmed"
                                    }
                                ]
                            },
                            {
                                "id": 2,
                                "type": "Credit Card",
                                "balance": "200",
                                "currency": "€",
                                "description": "Transport Credit Card",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 12.2,
                                        "description": "Combinado Metronorte",
                                        "status": "Confirmed"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "products": [
                            {
                                "id": 1,
                                "type": "Account",
                                "balance": "5000",
                                "currency": "€",
                                "description": "Account",
                                "listado": [
                                    {
                                        "transactionId": 1,
                                        "date": "26/11/2017",
                                        "amount": 33.95,
                                        "description": "A",
                                        "status": "Confirmed"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
]




{
    "id": "miguelangel.lopez.martinez@bbva.com",
    "name": "Miguel Angel",
    "apellidos": "Lopez Martinez",
    "accounts": [
        {
            "id": 1,
            "products": [
                {
                    "id": 1,
                    "type": "Account",
                    "balance": "5000",
                    "currency": "€",
                    "description": "Account",
                    "listado": [
                                {   "transactionId":1,
                                    "date": "26/11/2017",
                                    "amount": 33.95,
                                    "description": "A",
                                    "status":"Confirmed"
                                }
                            ]
                },
                {
                    "id": 2,
                    "type": "Credit Card",
                    "balance": "1200",
                    "currency": "€",
                    "description": "Credit Card",
                    "listado": [
                                {   "transactionId":1,
                                    "date": "26/11/2017",
                                    "amount": 20.19,
                                    "description": "Amazon",
                                    "status":"Confirmed"
                                }
                            ]
                },
                {
                    "id": 2,
                    "type": "Credit Card",
                    "balance": "200",
                    "currency": "€",
                    "description": "Restaurant Credit Card",
                    "listado": [
                                {   "transactionId":1,
                                    "date": "26/11/2017",
                                    "amount": 10.00,
                                    "description": "Restaurante Domino",
                                    "status":"Confirmed"
                                }
                            ]
                },
                {
                    "id": 2,
                    "type": "Credit Card",
                    "balance": "200",
                    "currency": "€",
                    "description": "Transport Credit Card",
                    "listado": [
                                {   "transactionId":1,
                                    "date": "26/11/2017",
                                    "amount": 12.20,
                                    "description": "Combinado Metronorte",
                                    "status":"Confirmed"
                                }
                            ]
                }
            ]    
        },
        {    "id": 2,
             "products": [
                {
                    "id": 1,
                    "type": "Account",
                    "balance": "5000",
                    "currency": "€",
                    "description": "Account",
                    "listado": [
                                {   "transactionId":1,
                                    "date": "26/11/2017",
                                    "amount": 33.95,
                                    "description": "A",
                                    "status":"Confirmed"
                                }
                            ]
                }
        	]
        }
        ]
}
          




var mongodb = require("mongodb");

var client = mongodb.MongoClient;
var url = "mongodb://host:port/local";

client.connect(url, function (err, db) {
    
    var collection = db.collection("movimientos");
    
    var query = {
        "id": "miguelangel.lopez.martinez@bbva.com"
    };
    
    var cursor = collection.find(query);
    
    cursor.forEach(
        function(doc) {
            console.log(doc["_id"]);
        },
        function(err) {
            db.close();
        }
    );
    
    // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/
    
});