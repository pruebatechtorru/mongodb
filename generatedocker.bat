rem build polymer
rem polymer build

rem kill old docker
docker kill mongodb

rem remove old docker
docker rm mongodb

rem create a new build
docker build -t torru/mongodb .

rem start new container
rem --net techui
docker run -v /c/mongodb:/data/db -p 27017:27017 --name mongodb -d torru/mongodb
